﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleCollider : MonoBehaviour
{
    public Hole hole;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<OVRGrabbable>() != null)
        {
            hole.Hit();
        }
    }
}
