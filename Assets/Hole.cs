﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour
{
    Animator _animator;
    public Material defaultMaterial, hitMaterial;
    public List<GameObject> cactusBody;

    public bool canHit;
    public float minDuration = 1.0f, maxDuration = 3.0f;
    private float duration;

    private Coroutine appearCoroutine;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        SetMaterial(defaultMaterial);
    }
  
    void SetMaterial(Material mat)
    {
        for (int i = 0; i < cactusBody.Count; i++)
            cactusBody[i].GetComponent<Renderer>().material = mat;
    }

    public void Show()
    {
        SetMaterial(defaultMaterial);
        _animator.SetTrigger("show");
        appearCoroutine = StartCoroutine(Appear());
        canHit = true;
    }

    IEnumerator Appear()
    {
        float timeElapsed = 0;
        duration = Random.Range(minDuration, maxDuration);
        while(timeElapsed < duration)
        {
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        Hide();
    }

    public void Hide()
    {
        if (appearCoroutine != null)
            StopCoroutine(appearCoroutine);

        HoleManager.Instance.RemoveActiveHole(this);
        _animator.SetTrigger("hide");
        canHit = false;
    }

    public void Hit()
    {
        if (!canHit)
            return;
        SetMaterial(hitMaterial);
        Hide();
        ScoreManager.Instance.AddScore(5);
    }
}
