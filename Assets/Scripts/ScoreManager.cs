﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : Singleton<ScoreManager>
{
    public delegate void ScoreUpdate(int score);
    public static event ScoreUpdate OnScoreUpdate;

    private int score = 0;

    public void AddScore(int value)
    {
        score += value;
        OnScoreUpdate?.Invoke(score);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            AddScore(5);
    }
}
