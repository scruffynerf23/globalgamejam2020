﻿using UnityEngine;
using System.Collections;

public interface IWindowEntry{
	string windowName{ get; set; }
}
