﻿using UnityEngine;
using System.Collections;

public interface IUIAnimation{

	string animationName{ get; set; }
	Animation anim{ get; set; }
	StartState startState{ get; set; }
	Direction animationDirection{ get; set; }
	EndState endState{ get; set; }
}
