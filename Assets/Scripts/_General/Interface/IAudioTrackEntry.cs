﻿using UnityEngine;
using System.Collections;

public interface IAudioTrackEntry 
{
	string trackName{get;set;}
	AudioTrackType audioTrackType{get;set;}
	bool isLooping{get;set;}
	FadeOptions fadeOption{get;set;}
}
