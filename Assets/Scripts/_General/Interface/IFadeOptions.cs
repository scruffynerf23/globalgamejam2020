﻿using UnityEngine;
using System.Collections;

public interface IFadeOptions 
{
	FadeType fadeType{get;set;}
	float fadeSpeed{get;set;}
	bool isFixedSpeed{get;set;}
	bool isFadeFromStart{get;set;}
	float fadeOutStart{get;set;}
	float fadeOutDuration{get;set;}
	float fadeInDuration{get;set;}
}
