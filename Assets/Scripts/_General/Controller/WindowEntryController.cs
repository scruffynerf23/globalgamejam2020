﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(UIAnimationController))]
public class WindowEntryController : MonoBehaviour, IWindowEntry {

	public bool doNotRegister;//set to true if window should not be added to ordered windows; ideal for windows that are only an "overlay"

	[SerializeField]private string _windowName;
	public string windowName{ get { return _windowName; } set { _windowName = value; } }

	[SerializeField] public UIAnimationController transitionAnimation;

	void OnEnable(){
		
	}
	public void TransitionIn(){
        GetAnimationController();
		transitionAnimation.TransitionIn ();
    }
	public void TransitionOut(){
        GetAnimationController();
        transitionAnimation.TransitionOut ();
	}

    void GetAnimationController()
    {
        if(transitionAnimation == null)
            transitionAnimation = GetComponent<UIAnimationController>();
    }
}
