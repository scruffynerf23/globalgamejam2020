﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrack3DEntryController : AudioTrackEntryController {

    public string trackID;

    protected override void Awake()
    {
        //base.Awake();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }

    protected override void Update()
    {
        base.Update();
    }

    public void Initialize3DAudioTrack()
    {
        AudioManager.updateMasterVol += UpdateVolume;
        switch (audioTrackType)
        {
            case AudioTrackType.BGM:
                MasterVolume();
                //AudioManager.updateMasterVol += UpdateVolume;
                AudioManager.updateBGMVol += UpdateVolume;
                AudioManager.muteAllAudio += Mute;
                AudioManager.muteAllBGM += Mute;

                //FadeSound();
                break;
            case AudioTrackType.SFX:
                //AudioManager.updateMasterVol += UpdateVolume;
                AudioManager.updateSFXVol += UpdateVolume;
                MasterVolume();
                AudioManager.muteAllAudio += Mute;
                AudioManager.muteAllSFX += Mute;
                //FadeSound();
                break;
            case AudioTrackType.Ambient:
                //AudioManager.updateMasterVol += UpdateVolume;
                MasterVolume();
                AudioManager.updateAmbientVol += UpdateVolume;
                AudioManager.muteAllAudio += Mute;
                AudioManager.muteAllAmbient += Mute;
                //FadeSound();
                break;
        }

        audioSource.Play();
        willUnregister = false;
    }

    public void Initialize3DAudioTrack(AudioTrackEntry aTrackEntry)
    {
        trackName = aTrackEntry.trackName;
        audioTrackType = aTrackEntry.audioTrackType;
        volumeModifier = aTrackEntry.volumeModifier;
        isLooping = aTrackEntry.isLooping;
        isPersist = aTrackEntry.isPersist;
        fadeOption = aTrackEntry.fadeOption;

        audioSource.clip = Resources.Load<AudioClip>("Audio/" + aTrackEntry.audioTrackType + "/" + aTrackEntry.trackName);
        AudioManager.updateMasterVol += UpdateVolume;
        switch (audioTrackType)
        {
            case AudioTrackType.BGM:
                MasterVolume();
                //AudioManager.updateMasterVol += UpdateVolume;
                AudioManager.updateBGMVol += UpdateVolume;
                AudioManager.muteAllAudio += Mute;
                AudioManager.muteAllBGM += Mute;

                FadeSound();
                break;
            case AudioTrackType.SFX:
                //AudioManager.updateMasterVol += UpdateVolume;
                AudioManager.updateSFXVol += UpdateVolume;
                MasterVolume();
                AudioManager.muteAllAudio += Mute;
                AudioManager.muteAllSFX += Mute;
                FadeSound();
                break;
            case AudioTrackType.Ambient:
                //AudioManager.updateMasterVol += UpdateVolume;
                MasterVolume();
                AudioManager.updateAmbientVol += UpdateVolume;
                AudioManager.muteAllAudio += Mute;
                AudioManager.muteAllAmbient += Mute;
                FadeSound();
                break;
        }

        audioSource.Play();
        willUnregister = false;
    }

    protected override void Unregister()
    {
        if (!FindObjectOfType(typeof(AudioManager))) return;
        AudioManager.updateMasterVol -= UpdateVolume;
        AudioManager.updateBGMVol -= UpdateVolume;
        AudioManager.updateSFXVol -= UpdateVolume;
        AudioManager.updateAmbientVol -= UpdateVolume;

        AudioManager.muteAllAudio -= Mute;
        AudioManager.muteAllBGM -= Mute;
        AudioManager.muteAllSFX -= Mute;
        AudioManager.muteAllAmbient -= Mute;

        StopAllCoroutines();

        /*
        if (!isPersist)
        {
            AudioManager.Instance.UnRegister3DTrack(this);
        }*/

    }
}
