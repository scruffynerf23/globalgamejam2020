﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardSettings : Singleton<KeyboardSettings> {

    const KeyCode MOVEUP = KeyCode.W;
    const KeyCode MOVEDOWN = KeyCode.S;
    const KeyCode MOVERIGHT = KeyCode.D;
    const KeyCode MOVELEFT = KeyCode.A;
    const KeyCode INCREASESCALE = KeyCode.Q;
    const KeyCode DECREASESCALE = KeyCode.E;
    const KeyCode ROTATE_POSITIVE_X = KeyCode.L;
    const KeyCode ROTATE_NEGATIVE_X = KeyCode.J;
    const KeyCode ROTATE_POSITIVE_Y = KeyCode.I;
    const KeyCode ROTATE_NEGATIVE_Y = KeyCode.K;
    const KeyCode ROTATE_POSITIVE_Z = KeyCode.U;
    const KeyCode ROATATEAXIS_NEGATIVE_Z = KeyCode.O;
    const KeyCode RESETROTATION = KeyCode.R;
    const KeyCode SAVECHANGES = KeyCode.Return;
    const KeyCode LOADCHANGES = KeyCode.Backspace;
    const KeyCode INCREASESPEED = KeyCode.RightArrow;
    const KeyCode DECREASESPEED = KeyCode.LeftArrow;
    const KeyCode INCREASESENSITIVITY = KeyCode.RightBracket;
    const KeyCode DECREASESENSITIVITY = KeyCode.LeftBracket;


    public KeyCode IncreaseSpeed = INCREASESPEED;
    public KeyCode DecreaseSpeed = DECREASESPEED;
    public KeyCode MoveUp = MOVEUP;
    public KeyCode MoveDown = MOVEDOWN;
    public KeyCode MoveRight = MOVERIGHT;
    public KeyCode MoveLeft = MOVELEFT;
    public KeyCode IncreaseScale = INCREASESCALE;
    public KeyCode DecreaseScale = DECREASESCALE;
    public KeyCode Rotate_positive_x = ROTATE_POSITIVE_X;
    public KeyCode Rotate_negative_x = ROTATE_NEGATIVE_X;
    public KeyCode Rotate_positive_y = ROTATE_POSITIVE_Y;
    public KeyCode Rotate_negative_y = ROTATE_NEGATIVE_Y;
    public KeyCode Rotate_positive_z = ROTATE_POSITIVE_Z;
    public KeyCode Rotate_negative_z = ROATATEAXIS_NEGATIVE_Z;
    public KeyCode ResetRotation = RESETROTATION;
    public KeyCode SaveChanges = SAVECHANGES;
    public KeyCode LoadChanges = LOADCHANGES;
    public KeyCode IncreaseSensitivity = INCREASESENSITIVITY;
    public KeyCode DecreaseSensitivity = DECREASESENSITIVITY;


    public void RevertToDefault()
    {
        IncreaseSpeed = INCREASESPEED;
        DecreaseSpeed = DECREASESPEED;
         MoveUp = MOVEUP;
         MoveDown = MOVEDOWN;
         MoveRight = MOVERIGHT;
         MoveLeft = MOVELEFT;
         IncreaseScale = INCREASESCALE;
         DecreaseScale = DECREASESCALE;
         Rotate_positive_x = ROTATE_POSITIVE_X;
         Rotate_negative_x = ROTATE_NEGATIVE_X;
         Rotate_positive_y = ROTATE_POSITIVE_Y;
         Rotate_negative_y = ROTATE_NEGATIVE_Y;
         Rotate_positive_z = ROTATE_POSITIVE_Z;
         Rotate_negative_z = ROATATEAXIS_NEGATIVE_Z;
        ResetRotation = RESETROTATION;
         SaveChanges = SAVECHANGES;
         LoadChanges = LOADCHANGES;
        IncreaseSensitivity = INCREASESENSITIVITY;
        DecreaseSensitivity = DECREASESENSITIVITY;
    }
}
