﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum TransitionState{In,Out}

public class GroupedUIAnimationController : MonoBehaviour {
	public string uiGroup;

	[Header("Transition In Settings")]
	public bool transitionInIndividually; //set to true if ui Objects should be transitioned one after the other
	public float transitionInDelay = 1.0f;
	//public float individualTransitionInTime;

	[Header("Transition Out Settings")]
	public bool transitionOutIndividually; //set to true if ui Objects should be transitioned one after the other
	public float transitionOutDelay = 1.0f;
	//public float individualTransitionOutTime;

	public List<UIAnimationController> uiObjects;

	private TransitionState currentState;

	void OnEnable(){
		WindowManager.OnTransitionIn += TransitionObjectsIn;
		WindowManager.OnTransitionOut += TransitionObjectsOut;
		InitializeUI ();
	}

	void OnDisable(){
		WindowManager.OnTransitionIn -= TransitionObjectsIn;
		WindowManager.OnTransitionOut -= TransitionObjectsOut;
	}

	public void InitializeUI(){
		foreach (UIAnimationController ui in uiObjects) {//windows need to be enabled at start for values to be initialized
				ui.gameObject.SetActive (true);
		}

		foreach (UIAnimationController ui in uiObjects) {//windows need to be enabled at start for values to be initialized
			if (ui.gameObject.activeInHierarchy)
				ui.gameObject.SetActive (false);
		}
	}

    public void StartTransitionIn()
    {
        Debug.Log("in");
        currentState = TransitionState.In;
        TransitionObjects();
    }

    public void StartTransitionOut()
    {
        currentState = TransitionState.Out;
        TransitionObjects();
    }
    private void TransitionObjectsIn(string groupName){
		StartCoroutine ("TransitionInDelay", groupName);
	}

	private void TransitionObjectsOut(string groupName){
		WindowManager.Instance.SetTransitionOutDelay (transitionOutDelay);
		TransitionByUiGroup (groupName, TransitionState.Out);
	}

	private IEnumerator TransitionInDelay(string groupName){
		yield return new WaitForSeconds (transitionInDelay);
		TransitionByUiGroup (groupName, TransitionState.In);
	}

	private void TransitionByUiGroup(string groupName, TransitionState state){
		if (uiGroup == groupName) {
			currentState = state;
			TransitionObjects ();
		}
	}
		
	private void TransitionObjects(){
		if (currentState == TransitionState.In) {
			if (transitionInIndividually)
				TransitionUntilFull ();
			else {
				foreach (UIAnimationController ui in uiObjects) {
					ui.TransitionIn ();
				}
			}
		} 
		else if (currentState == TransitionState.Out) {
			if (transitionOutIndividually)
				TransitionUntilFull ();
			else {
				foreach (UIAnimationController ui in uiObjects) {
					ui.TransitionOut ();
				}
			}
		}
	}
		
	private UIAnimationController NextTransition(){
		foreach (UIAnimationController ui in uiObjects) {//loop over every ui objects
			//set bool according to state
			bool transitioned = (currentState == TransitionState.In) ? ui.transitionInAnimation.isTransitioned : ui.transitionOutAnimation.isTransitioned;
			if (!transitioned) {//check if ui has not yet transitioned
				return ui;//if not yet, then this must be the next ui to be transitioned
			}
		}
		return
			null;
	}

	private void TransitionUntilFull(){
		UIAnimationController uiController = NextTransition ();
		if (uiController != null) {//check if there is an existing ui that has not yet transitioned
			if (currentState == TransitionState.In)//transition based on state
				uiController.TransitionIn ();
			else
				uiController.TransitionOut ();
		}
		if (NextTransition()) {//if there is an existing ui that has not yet transitioned, we wait before calling the method again
			if (currentState == TransitionState.In) {//invoke method again after the time of transition based on the state
				Invoke ("TransitionUntilFull", uiController.transitionInAnimation.GetTransitionTime());
			} else {
				Invoke ("TransitionUntilFull", uiController.transitionOutAnimation.GetTransitionTime());
			}
		}

		if (NextTransition() == null) {
			WindowManager.Instance.isTransitioningIn = false;
		}
	}

}
