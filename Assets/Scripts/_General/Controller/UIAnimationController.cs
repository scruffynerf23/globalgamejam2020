﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public enum PanelType { Image, Text}
public class UIAnimationController:MonoBehaviour {
	
	public UIAnimation transitionInAnimation;
	public UIAnimation transitionOutAnimation;

	private UIAnimation currentAnim;

	private Image panelImage;
    private Text panelText;
	System.Action eventCallBack;

    private float alphaTransparency;
    private PanelType panelType;

    bool isInitialized = false;

	void OnEnable(){
       
		Initialize ();
	}

	public void Initialize(){
        if (!isInitialized)
        {
            if (GetComponent<Image>() != null)
            {
                panelType = PanelType.Image;
                panelImage = GetComponent<Image>();
                alphaTransparency = panelImage.color.a;
            }
            else if (GetComponent<Text>() != null)
            {
                panelType = PanelType.Text;
                panelText = GetComponent<Text>();
                alphaTransparency = panelText.color.a;
            }

            AutoFillState(transitionInAnimation);
            AutoFillState(transitionOutAnimation);
            isInitialized = true;
        }

    }

	private void AutoFillState(UIAnimation transition){
		if (transition.isAuto) {
			//auto set values to avoid inconsistency of start state with desired transition effect
			if (transition.targetGameObject == null)
				transition.targetGameObject = this.gameObject;
			if (transition.isPlayAnim)
				transition.startState = StartState.Enable;
			if (transition.isScaled && transition == transitionInAnimation)
				transition.startState = StartState.ScaleIn;
			else if (transition.isScaled && transition == transitionOutAnimation) {
				transitionOutAnimation.startState = StartState.ScaleOut;
				transitionOutAnimation.endState = EndState.Disable;
			}
			if (transition.isFade && transition == transitionInAnimation)
				transition.startState = StartState.FadeIn;
			else if (transitionOutAnimation.isFade && transition == transitionOutAnimation) {
				transitionOutAnimation.startState = StartState.FadeOut;
				transitionOutAnimation.endState = EndState.Disable;
			}
			if (transition.isScaled && transition.isFade && transition == transitionInAnimation) {
				transition.startState = StartState.ScaleAndFadeIn;
			} else if (transition.isScaled && transition.isFade && transition == transitionOutAnimation) {
				transitionOutAnimation.startState = StartState.ScaleAndFadeOut;
				transitionOutAnimation.endState = EndState.Disable;
			}
		}
	}

	public void TransitionIn(){
		ResetTransitions ();
		Play (transitionInAnimation);
	}
	public void TransitionOut(){
		ResetTransitions ();
		Play (transitionOutAnimation);
	}


	private void ResetTransitions(){
		transitionInAnimation.isTransitioned = false;
		transitionOutAnimation.isTransitioned = false;
	}
	protected void Play(UIAnimation windowAnim){
		currentAnim = windowAnim;
		EnterStartState ();
		if(currentAnim.isPlayAnim && this.gameObject.activeInHierarchy)
			StartCoroutine ("PlayAnim");
	}

	protected IEnumerator PlayAnim(){
        currentAnim.anim = GetComponent<Animation>();
		PlayAnimation ();
		currentAnim.anim.Play ();
		while (currentAnim.anim.IsPlaying(currentAnim.animationName)){
			yield return null;
		}
		EnterEndState ();
	}


	private void PlayAnimation(){
		currentAnim.anim.clip = currentAnim.anim [currentAnim.animationName].clip;
		switch (currentAnim.animationDirection) {
		case Direction.Forward:
			currentAnim.anim [currentAnim.animationName].time = 0;
			currentAnim.anim [currentAnim.animationName].speed = 1;
			break;
		case Direction.Reverse:
			currentAnim.anim [currentAnim.animationName].time = currentAnim.anim [currentAnim.animationName].length;
			currentAnim.anim [currentAnim.animationName].speed = -1;
			break;
		}
	}

	private void EnterStartState(){
		currentAnim.isTransitioned = true;
		switch (currentAnim.startState) {
		case StartState.Enable:
			currentAnim.targetGameObject.SetActive (true);
			break;
		case StartState.Disable:
			currentAnim.targetGameObject.SetActive (false);
			break;
		case StartState.ScaleIn:
			currentAnim.targetGameObject.transform.localScale = Vector3.zero;
			currentAnim.targetGameObject.SetActive (true);
			ScaleIn (EnterEndState);
			break;
		case StartState.ScaleOut:
			currentAnim.targetGameObject.SetActive (true);
			ScaleOut (EnterEndState);
			break;
		case StartState.FadeIn:
			currentAnim.targetGameObject.SetActive (true);
			StartCoroutine ("FadeTransition", StartState.FadeIn);
			break;
		case StartState.FadeOut:
			currentAnim.targetGameObject.SetActive (true);
			StartCoroutine ("FadeTransition", StartState.FadeOut);
			break;
		case StartState.ScaleAndFadeIn:
			currentAnim.targetGameObject.transform.localScale = Vector3.zero;
			currentAnim.targetGameObject.SetActive (true);
			ScaleAndFade (StartState.ScaleAndFadeIn);
			break;
		case StartState.ScaleAndFadeOut:
			currentAnim.targetGameObject.transform.localScale = Vector3.zero;
			currentAnim.targetGameObject.SetActive (false);
			ScaleAndFade (StartState.ScaleAndFadeIn);
			break;
		case StartState.None:
			break;
		}
		currentAnim.startEvent.Invoke ();
	}

	private void EnterEndState(){
		switch (currentAnim.endState) {
		case EndState.Enable:
			currentAnim.targetGameObject.SetActive (true);
			break;
		case EndState.Disable:
			currentAnim.targetGameObject.SetActive (false);
			break;
		case EndState.None:
			break;
		}
		currentAnim.endEvent.Invoke ();
	}

	private void ScaleAndFade(StartState transition){
		ScaleIn (EnterEndState);
		StartCoroutine ("FadeTransition", transition);
	}

	private void ScaleIn(System.Action cb){
		eventCallBack = cb;
		StartCoroutine ("ScaleTransition", StartState.ScaleIn);
	}

	private void ScaleOut(System.Action cb){
		eventCallBack = cb;
		StartCoroutine ("ScaleTransition", StartState.ScaleOut);
	}

	private IEnumerator ScaleTransition(StartState transition){
		float elapsedTime = 0;
		Vector3 startScale = currentAnim.targetGameObject.transform.localScale;
		Vector3 endScale = (transition == StartState.ScaleIn) ? Vector3.one : Vector3.zero;
		while(elapsedTime < currentAnim.windowScaleDuration)
		{
			currentAnim.targetGameObject.transform.localScale = Vector3.Lerp (startScale, endScale, elapsedTime / currentAnim.windowScaleDuration);
			elapsedTime += Time.deltaTime;
			yield return null;
		}
		eventCallBack ();
	}

	IEnumerator FadeTransition(StartState transition){
		//float startAlpha = panelImage.color.a;
		float alpha = (transition == StartState.FadeOut) ? alphaTransparency : 0.0f; //set alpha to one if fadeout and 0 if not
		float fadeEndValue = (transition == StartState.FadeOut) ? 0.0f : alphaTransparency;
		if (transition == StartState.FadeOut) {
			while (alpha >= fadeEndValue) {
				SetAlphaTransition (ref alpha, StartState.FadeOut);
				yield return null;
			}
			EnterEndState ();
		} else {
			while (alpha <= fadeEndValue) {
				SetAlphaTransition (ref alpha, StartState.FadeIn);
				yield return null;
			}
			EnterEndState ();
		}
	}

	void SetAlphaTransition(ref float alpha, StartState trans){
        switch (panelType)
        {
            case PanelType.Image:
                panelImage.color = new Color(panelImage.color.r, panelImage.color.g, panelImage.color.b, alpha);
                break;
            case PanelType.Text:
                panelText.color = new Color(panelText.color.r, panelText.color.g, panelText.color.b, alpha);
                break;
        }
		
		//WindowManager.Instance.SetTransitionOutDelay (currentAnim.fadeDuration);
		alpha += Time.deltaTime * (1.0f / currentAnim.fadeDuration) * ((trans == StartState.FadeOut)? -1 : 1) ;
	}
}
