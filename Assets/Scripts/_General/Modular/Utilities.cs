﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public static class Utilities {

    public static string GetDictionaryKeyValue(string key, Dictionary<string, object> entry)
    {
        if (entry.ContainsKey(key)
            && entry[key] != null
            && !string.IsNullOrEmpty(entry[key].ToString()))
        {
            return entry[key].ToString();
        }
        else
            return string.Empty;
    }

    public static Dictionary<string,object> GetDictionaryObject(string key, Dictionary<string,object> entry)
    {
        var objectValue = entry[key] as object;
        if (objectValue != null)
            return objectValue as Dictionary<string, object>;
        else
            return null;
       
    }

    public static bool ParseBool(string key, Dictionary<string, object> entry)
    {
        bool tempBool = false;
        if (entry.ContainsKey(key) && entry[key] != null && !string.IsNullOrEmpty(entry[key].ToString()))
            bool.TryParse(entry[key].ToString(), out tempBool);
        return tempBool;
    }

    public static float ParseFloat(string key, Dictionary<string, object> entry)
    {
        float tempFloat = 0;
        if (entry.ContainsKey(key) && entry[key] != null && !string.IsNullOrEmpty(entry[key].ToString()))
            float.TryParse(entry[key].ToString(), out tempFloat);
        return tempFloat;
    }

    public static int ParseInt(string key, Dictionary<string, object> entry)
    {
        int tempInt = 0;
        if (entry.ContainsKey(key) && entry[key] != null && !string.IsNullOrEmpty(entry[key].ToString()))
            int.TryParse(entry[key].ToString(), out tempInt);
        return tempInt;
    }

    public static Sprite GetSpriteFromResources(string key, Dictionary<string, object> entry)
    {
        string pledgeImageText = Utilities.GetDictionaryKeyValue(key, entry);
        if (pledgeImageText != string.Empty)
            return Resources.Load<Sprite>("Sprites/" + pledgeImageText);
        else
            return null;
    }

    public static Sprite GetSpriteFromResources(string location, string spriteName)
    {
        return Resources.Load<Sprite>(location + spriteName);
    }

    public static double ParseDouble(string key, Dictionary<string, object> entry)
    {
        double tempDouble = 0;
        if (entry.ContainsKey(key) && entry[key] != null && !string.IsNullOrEmpty(entry[key].ToString()))
            double.TryParse(entry[key].ToString(), out tempDouble);
        return tempDouble;
    }

    public static void WriteDataToFile(string jsonString)
    {
        string path = Application.dataPath + "/Resources/Files/Data.json";
        File.WriteAllText(path, jsonString);
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
    }

    public static KeyCode DetectPressedKeyOrButton()
    {
        foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(kcode))
                return kcode;
        }
        return KeyCode.None;
    }

    public static string GetKeyCodeString(KeyCode kcode)
    {

        if (kcode == KeyCode.Alpha0)
            return "0";
        else if (kcode == KeyCode.Alpha1)
            return "1";
        else if (kcode == KeyCode.Alpha2)
            return "2";
        else if (kcode == KeyCode.Alpha3)
            return "3";
        else if (kcode == KeyCode.Alpha4)
            return "4";
        else if (kcode == KeyCode.Alpha5)
            return "5";
        else if (kcode == KeyCode.Alpha6)
            return "6";
        else if (kcode == KeyCode.Alpha7)
            return "7";
        else if (kcode == KeyCode.Alpha8)
            return "8";
        else if (kcode == KeyCode.Alpha9)
            return "9";
        else if (kcode == KeyCode.BackQuote)
            return "~";
        else
            return kcode.ToString();
     
    }


}
