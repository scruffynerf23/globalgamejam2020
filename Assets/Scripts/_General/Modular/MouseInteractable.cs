﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class MouseInteractable : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, IDragHandler, IDropHandler, IBeginDragHandler, IEndDragHandler
{
	public delegate void PointerAction();
	public static event PointerAction OnPointEnter;
	public static event PointerAction OnPointExit;
	public static event PointerAction OnPointDown;
	public static event PointerAction OnPointUp;
	public static event PointerAction OnPointDrag;
	public static event PointerAction OnPointDrop;
	public static event PointerAction OnPointBeginDrag;
	public static event PointerAction OnPointEndDrag;

	public delegate void MouseAction ();
	public static event MouseAction MouseDown;
	public static event MouseAction MouseDrag;
	public static event MouseAction MouseUp;

	public delegate bool IsAcceptable(GameObject obj);

	private bool interact;

	public virtual void OnDrag(PointerEventData data){if (OnPointDrag != null)OnPointDrag ();}
	public virtual void OnDrop(PointerEventData data){if (OnPointDrop != null)OnPointDrop ();}
	public virtual void OnBeginDrag(PointerEventData data){if (OnPointBeginDrag != null)OnPointBeginDrag ();}
	public virtual void OnEndDrag(PointerEventData data){if (OnPointEndDrag != null)OnPointEndDrag ();}
	public virtual void OnPointerDown(PointerEventData data){if (OnPointDown != null)OnPointDown ();}
	public virtual void OnPointerUp(PointerEventData data){if (OnPointUp != null)OnPointUp ();}
	public virtual void OnPointerEnter(PointerEventData data){if (OnPointEnter != null) OnPointEnter ();}
	public virtual void OnPointerExit(PointerEventData data){if (OnPointExit != null) OnPointExit ();}

	public virtual void OnMouseDown(){if (MouseDown != null)MouseDown ();}
	public virtual void OnMouseDrag(){if (MouseDrag != null)MouseDrag ();}
	public virtual void OnMouseUp(){if (MouseUp != null)MouseUp ();}

}
