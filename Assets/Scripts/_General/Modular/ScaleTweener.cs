﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class ScaleTweener : MouseInteractable {
	[SerializeField] Vector3 /*hoverScale,*/ pressScale;
	[SerializeField] float duration;

	public GameObject target;

	RectTransform myTrans;

	private float t;
	private Vector3 origScale;

	private bool isInteract;
	private Button myButton;

	void OnEnable()
	{
		OnPointDown += TweenScaleOnPress;
		OnPointUp += TweenScaleReset;
		if (target == null) {
			myTrans = this.GetComponent<RectTransform> ();
		} else {
			myTrans = target.GetComponent<RectTransform> ();
		}
		myButton = this.GetComponent<Button> ();
		origScale = myTrans.localScale;
	}

	void OnDisable(){
		OnPointDown -= TweenScaleOnPress;
		OnPointUp -= TweenScaleReset;
		myTrans.localScale = origScale;
	}

	IEnumerator TweenScale(Vector3 vec)
	{
		t = 0;
		while(myTrans.localScale != vec)
		{
			myTrans.localScale = Vector3.Lerp(myTrans.localScale, vec, t);
			if(t < 1)
				t += Time.deltaTime/duration;
			yield return null;
		}
	}

	public override void OnPointerDown(PointerEventData data){
		isInteract = true;
		base.OnPointerDown (data);
	}

	public override void OnPointerUp(PointerEventData data){
		isInteract = false;
		base.OnPointerUp (data);
	}

	public void TweenScaleOnPress()
	{
		StopAllCoroutines ();
		if(isInteract && myButton.IsInteractable())
			StartCoroutine("TweenScale", pressScale);
	}

	public void TweenScaleReset()
	{
		StopAllCoroutines ();
		StartCoroutine("TweenScale", origScale);
	}

	public void ResetScale(){
		myTrans.localScale = Vector3.one;
	}
}
