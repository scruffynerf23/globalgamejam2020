﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public enum StartState{Enable, Disable, ScaleIn, ScaleOut, FadeIn, FadeOut, ScaleAndFadeIn, ScaleAndFadeOut, None}
public enum Direction{Forward,Reverse}
public enum EndState{Enable, Disable, None}


[System.Serializable]
public class UIAnimation: IUIAnimation {
	private Animation _anim;
	private GameObject _gameObject;

	[SerializeField]private GameObject _targetGameObject;

	[Header("Auto")]
	public bool isAuto = true;//setting this value to true automatically fills up the best start and end state for the transition

	[Header("Animation")]
	[SerializeField]private bool _isPlayAnim;
	[ConditionalHideAttribute("_isPlayAnim",true)]
	[SerializeField]private string _animationName;
	[ConditionalHideAttribute("_isPlayAnim",true)]
	[SerializeField]private Direction _animationDirection;

	[Header("Scale")]
	public bool isScaled;

	[ConditionalHideAttribute("isScaled",true)]
	[SerializeField]private float _windowScaleDuration = 1.0f;

	[Header("Fade")]
	public bool isFade;
	[ConditionalHideAttribute("isFade",true)]
	public float fadeDuration = 1.0f;

	[Header("States")]
	[ConditionalHideAttribute("isAuto",false,true)]
	[SerializeField]private StartState _startState;
	[ConditionalHideAttribute("isAuto",false,true)]
	[SerializeField]private EndState _endState;

	public UnityEvent startEvent,endEvent;

	private bool _isTransitioned;
	private float transitionTime;

	public float GetTransitionTime(){
		if (isFade)
			return fadeDuration;
		else if (isPlayAnim)
			return anim.clip.length;
		else if (isScaled)
			return windowScaleDuration;
		else
			return 0;
	}

	public GameObject targetGameObject{get { return _targetGameObject; } set{ _targetGameObject = value; }}
	public GameObject gameObject{get { return _gameObject; } set{ _gameObject = value; }}
	public Animation anim{ get { return _anim; } set { _anim = value; } }
	public string animationName{ get { return _animationName; } set { _animationName = value; } }
	public StartState startState{ get { return _startState; } set { _startState = value; } }
	public Direction animationDirection{ get { return _animationDirection; } set { _animationDirection = value; } }
	public EndState endState{ get { return _endState; } set { _endState = value; } }
	public float windowScaleDuration{ get { return _windowScaleDuration; } set { _windowScaleDuration = value; } }
	public bool isPlayAnim{ get { return _isPlayAnim; } set { _isPlayAnim = value; } }
	public bool isTransitioned{ get { return _isTransitioned; } set { _isTransitioned = value; } }
}
