﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleAnimation : MonoBehaviour {

	public Animator anim;
	public string boolString;
	public bool isEnabled;

	public void ToggleAnim(){
		isEnabled = !isEnabled;
		anim.SetBool (boolString, isEnabled);
	}
}
