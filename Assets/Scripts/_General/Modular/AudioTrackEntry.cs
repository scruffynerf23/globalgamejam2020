﻿using UnityEngine;
using System.Collections;

public enum AudioTrackType { BGM, SFX, Ambient }
public enum FadeType { None, In, Out, Both }

[System.Serializable]
public class AudioTrackEntry : IAudioTrackEntry
{
    #region IAudioTrackEntry implementation
    [SerializeField] string _trackName;
    AudioTrackType _audioTrackType;
    [Range(0f, 1f)]
    [SerializeField] float _volumeModifier = 1f;
    [SerializeField] bool _isLooping;
    [SerializeField] bool _isPersist;
    [SerializeField]
    int _amountToPool = 1;
    [SerializeField]
    bool _shouldExpand;
    [SerializeField]
    Transform _parentObject;
    [SerializeField] FadeOptions _fadeOption;


    public string trackName { get { return _trackName; } set { _trackName = value; } }
    public AudioTrackType audioTrackType { get { return _audioTrackType; } set { _audioTrackType = value; } }
    public float volumeModifier { get { return _volumeModifier; } set { _volumeModifier = value; } }
    public bool isLooping { get { return _isLooping; } set { _isLooping = value; } }
    public bool isPersist { get { return _isPersist; } set { _isPersist = value; } }
    public int amountToPool { get { return _amountToPool; } set { _amountToPool = value; } }
    public bool shouldExpand { get { return _shouldExpand; } set { _shouldExpand = value; } }
    public Transform parentTransform { get { return _parentObject; } set { _parentObject = value; } }
    public FadeOptions fadeOption { get { return _fadeOption; } set { fadeOption = value; } }


    #endregion
}
