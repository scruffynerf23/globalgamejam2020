﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using System;

public class AnalyticsManager : Singleton<AnalyticsManager> {

    protected override void Awake()
    {
        base.Awake();
        OnGameInit();
    }

    public void OnGameInit()
    {
        Debug.Log("initialized event");
        Analytics.CustomEvent("gameInitialized", new Dictionary<string, object>
        {
            {"time", System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")} 
        });
        
    }

    public void OnGameQuit()
    {
        Debug.Log("quit event");
        Analytics.CustomEvent("gameQuit", new Dictionary<string, object>
        {
            {"time", System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")},
        });
       
    }

    protected override void OnApplicationQuit()
    {
        base.OnApplicationQuit();
        OnGameQuit();
    }

}
