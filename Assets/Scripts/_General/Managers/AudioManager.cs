﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AudioManager : Singleton<AudioManager>
{
    [Range(0f, 1f)]
    [SerializeField] float _masterVolume = 1.0f;
    [Range(0f, 1f)]
    [SerializeField] float _backgroundMusicVolue = 1.0f;
    [Range(0f, 1f)]
    [SerializeField] float _soundEffectsVolume = 1.0f;
    [Range(0f, 1f)]
    [SerializeField] float _ambientSoundVolume = 1.0f;

    public float sfxModifier = 1.0f, ambientModifier = 1.0f;

    public float masterVolume { get { return _masterVolume; } set { _masterVolume = value; } }
    public float soundEffectsVolume { get { return _soundEffectsVolume; } set { _soundEffectsVolume = value; } }
    public float ambientSoundVolume { get { return _ambientSoundVolume; } set { _ambientSoundVolume = value; } }
    public float backgroundMusicVolume { get { return _backgroundMusicVolue; } set { _backgroundMusicVolue = value; } }

    public List<AudioTrackEntry> bgmTracks = new List<AudioTrackEntry>();
    public List<AudioTrackEntry> sfxTracks = new List<AudioTrackEntry>();
    public List<AudioTrackEntry> ambientTracks = new List<AudioTrackEntry>();

    public List<AudioTrackEntryController> audioTrackEntryControllers = new List<AudioTrackEntryController>();

    public delegate void updateVolume(float value);
    public static event updateVolume updateMasterVol;
    public static event updateVolume updateBGMVol;
    public static event updateVolume updateSFXVol;
    public static event updateVolume updateAmbientVol;

    public delegate void muteAudio(bool isMute);
    public static event muteAudio muteAllAudio;
    public static event muteAudio muteAllBGM;
    public static event muteAudio muteAllSFX;
    public static event muteAudio muteAllAmbient;


    protected override void Awake()
    {
        base.Awake();
        SetAudioTrackEntriesType();
    }

    private void OnEnable()
    {
        //ProgressManager.OnGameRestart += Restart;
    }

    private void OnDisable()
    {
        //ProgressManager.OnGameRestart -= Restart;
    }

    private void SetAudioTrackEntriesType()
    {
        foreach (AudioTrackEntry ate in bgmTracks)
        {
            ate.audioTrackType = AudioTrackType.BGM;
        }

        foreach (AudioTrackEntry ate in sfxTracks)
        {
            ate.audioTrackType = AudioTrackType.SFX;
        }

        foreach (AudioTrackEntry ate in ambientTracks)
        {
            ate.audioTrackType = AudioTrackType.Ambient;
        }
    }

    public void UpdateMasterVolume(float value)
    {
        if (updateMasterVol != null) updateMasterVol(value);
        masterVolume = value;
        //DataManager.Instance.masterVolume = value;
    }

    public void UpdateBGMVolume(float value)
    {
        if (updateBGMVol != null) updateBGMVol(value);
        backgroundMusicVolume = value;
        //DataManager.Instance.bgmVolume = value;
    }

    public void UpdateSFXVolume(float value)
    {
        if (updateSFXVol != null) updateSFXVol(value);
        soundEffectsVolume = value;
        //DataManager.Instance.sfxVolume = value;
    }

    public void UpdateAmbientVolume(float value)
    {
        if (updateAmbientVol != null) updateAmbientVol(value);
        ambientSoundVolume = value;
        //DataManager.Instance.ambientVolume = value;
    }


    public void changeMasterVolume(float value)
    {
        masterVolume = value;

    }
    public void MuteAll(bool isMute)
    {
        if (muteAllAudio != null) muteAllAudio(isMute);
        PlayerPrefs.SetInt("MuteAll", (isMute) ? 1 : 0);
    }
    public void MuteBGMs(bool isMute)
    {
        if (muteAllBGM != null) muteAllBGM(isMute);
        PlayerPrefs.SetInt("MuteBgm", (isMute) ? 1 : 0);
    }
    public void MuteSFXs(bool isMute)
    {
        if (muteAllSFX != null) muteAllSFX(isMute);
        PlayerPrefs.SetInt("MuteSFX", (isMute) ? 1 : 0);
    }
    public void MuteAmbients(bool isMute)
    {
        if (muteAllAmbient != null) muteAllAmbient(isMute);
        PlayerPrefs.SetInt("MuteAmbient", (isMute) ? 1 : 0);
    }

    public bool muteAllAud
    {
        get { return (PlayerPrefs.GetInt("MuteAll", 0) == 1) ? true : false; }
    }
    public bool muteBGM
    {
        get { return (PlayerPrefs.GetInt("MuteBgm", 0) == 1) ? true : false; }
    }
    public bool muteSFX
    {
        get { return (PlayerPrefs.GetInt("MuteSFX", 0) == 1) ? true : false; }
    }
    public bool muteAmbient
    {
        get { return (PlayerPrefs.GetInt("MuteAmbient", 0) == 1) ? true : false; }
    }

    public void RemoveSound()
    {
        UpdateMasterVolume(0f);
        UpdateBGMVolume(0f);
        UpdateSFXVolume(0f);
        UpdateAmbientVolume(0f);
    }

    public void ReturnSound()
    {
        UpdateMasterVolume(masterVolume);
        UpdateBGMVolume(backgroundMusicVolume);
        UpdateSFXVolume(soundEffectsVolume);
        UpdateAmbientVolume(ambientSoundVolume);
    }

    public int GetCountInList(string audioName)
    {
        int count = 0;
        foreach (AudioTrackEntryController atec in audioTrackEntryControllers)
        {
            if (atec.trackName == audioName)
                count++;
        }

        return count;
    }
    private AudioTrackEntry FindAudioTrackEntry(string trackname)
    {
        AudioTrackEntry ate = bgmTracks.Find(track => track.trackName == trackname);
        //Debug.Log("finding " + trackname + " in bgm");
        if (ate == null)
        {
            //Debug.Log(trackname + "is not found in bgm");
            ate = sfxTracks.Find(track => track.trackName == trackname);
            //Debug.Log("finding " + trackname + " in sfx");
            if (ate == null)
            {
                //Debug.Log(trackname + "is not found in sfx");
                ate = ambientTracks.Find(track => track.trackName == trackname);
                //Debug.Log("finding " + trackname + " in ambient");
                if (ate == null)
                {
                    //Debug.LogError(trackname + " does not exist in entries.");
                    return null;
                }
                else
                    return ate;
            }
            else
                return ate;
        }
        else
            return ate;

    }

    public AudioTrackEntryController PlayTrack(string trackname)
    {
        AudioTrackEntry ate = FindAudioTrackEntry(trackname);
        if (ate == null)
        {
            Debug.LogError("Audio track [" + trackname + "] not found");
            return null;
        }

        /*
		GameObject go = Instantiate( Resources.Load("AudioTrack" , typeof(GameObject)) ) as GameObject ;

		AudioTrackEntryController atec = go.GetComponent<AudioTrackEntryController>() ;
		audioTrackEntryControllers.Add( atec ) ;

		atec.InitializeAudioTrack( ate ) ;
        */
        GameObject go = ObjectPoolManager.Instance.GetPooledAudio(trackname);

        if (go == null)
        {
            //Debug.Log("no available pooled object");
            return null;
        }


        AudioTrackEntryController atec = go.GetComponent<AudioTrackEntryController>();
        go.SetActive(true);
        audioTrackEntryControllers.Add(atec);

        UpdateMasterVolume(masterVolume);
        UpdateBGMVolume(backgroundMusicVolume);
        UpdateSFXVolume(soundEffectsVolume);
        UpdateAmbientVolume(ambientSoundVolume);

        MuteAll(muteAllAud);
        MuteBGMs(muteBGM);
        MuteSFXs(muteSFX);
        MuteAmbients(muteAmbient);

        atec.Play();
        return atec;
    }

    public void FadeTrackToStop(string trackname, float fadeSpeed = 1.0f)
    {
        AudioTrackEntryController atec = audioTrackEntryControllers.Find(track => track.trackName == trackname);
        if (atec != null)
        {
            atec.FadeOutOnStop(fadeSpeed);
        }
    }

    public void StopTrack(string trackname)
    {
        AudioTrackEntryController atec = audioTrackEntryControllers.Find(track => track.trackName == trackname);
        if (atec != null) atec.Stop();
    }

    public void StopAllTrack()
    {
        for (int i = 0; i < audioTrackEntryControllers.Count; i++)
        {
            audioTrackEntryControllers[i].Stop();
        }
    }

    public AudioTrackEntryController GetTrack(string trackname)
    {
        AudioTrackEntryController atec = audioTrackEntryControllers.Find(track => track.trackName == trackname);
        if (atec.trackName == trackname)
            return atec;
        else
            return null;
    }

    //new
    public AudioClip GetTrackAudioClip(string trackname)
    {
        AudioTrackEntryController atec = audioTrackEntryControllers.Find(track => track.trackName == trackname);
        if (atec.trackName == trackname)
            return atec.audioSource.clip;
        else
            return null;
    }

    public AudioSource GetTrackAudioSource(string trackname)
    {
        AudioTrackEntryController atec = audioTrackEntryControllers.Find(track => track.trackName == trackname);
        if (atec.trackName == trackname)
            return atec.audioSource;
        else
            return null;
    }

    public void PauseTrack(string trackname, bool paused)
    {
        AudioTrackEntryController atec = audioTrackEntryControllers.Find(track => track.trackName == trackname);
        if (atec.trackName == trackname)
        {
            atec.PauseTrack(paused);
        }
    }

    public void PauseAllTrack(bool paused)
    {
        for (int i = 0; i < audioTrackEntryControllers.Count; i++)
        {
            audioTrackEntryControllers[i].PauseTrack(paused);
        }
    }

    public bool ExistsInRegistry(AudioTrackEntryController atec)
    {
        if (audioTrackEntryControllers.Contains(atec))
            return true;
        else
            return false;
    }
    public bool ExistsInRegistry(string trackName)
    {
        AudioTrackEntryController atec = audioTrackEntryControllers.Find(track => track.trackName == trackName);
        if (audioTrackEntryControllers.Contains(atec))
            return true;
        else
            return false;
    }

    public void UnRegisterTrack(AudioTrackEntryController atec)
    {
        if (audioTrackEntryControllers.Contains(atec))
        {
            //Destroy( atec.gameObject ) ;
            atec.gameObject.SetActive(false);
            audioTrackEntryControllers.Remove(atec);
        }
    }

    public void UnRegister3DTrack(AudioTrackEntryController atec)
    {
        //Debug.Log("unregistering track");
        if (audioTrackEntryControllers.Contains(atec))
        {
            audioTrackEntryControllers.Remove(atec);
        }
        //RefreshUnregister();
    }

    void RefreshUnregister()
    {
        foreach (AudioTrackEntryController atec in audioTrackEntryControllers)
        {
            if (atec.IsUnregister())
            {
                UnRegister3DTrack(atec);
            }
        }
    }
    public void CheckTrackInRegistry(string trackName, System.Action callback)
    {
        if (ExistsInRegistry(trackName))
            callback();
    }

    public void ClearRegistry()
    {
        for (int i = 0; i < audioTrackEntryControllers.Count; i++)
        {
            UnRegisterTrack(audioTrackEntryControllers[i]);
        }
    }

    public bool IsTrackPlaying(string trackName)
    {
        AudioTrackEntryController atec = audioTrackEntryControllers.Find(track => track.trackName == trackName);
        if (atec != null)
            return atec.isPlaying;
        return false;
    }

    public void SetTrackPersistence(string trackName, bool value)
    {
        AudioTrackEntryController atec = audioTrackEntryControllers.Find(track => track.trackName == trackName);
        if (atec != null) atec.SetPersistence(value);
    }

    public void SetTrackPitch(string trackName, float pitchValue)
    {
        AudioTrackEntryController atec = audioTrackEntryControllers.Find(track => track.trackName == trackName);
        if (atec != null) atec.SetPitch(pitchValue);
    }

    private void Restart()
    {
        audioTrackEntryControllers.Clear();
    }

}
