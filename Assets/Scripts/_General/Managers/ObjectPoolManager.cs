﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : Singleton<ObjectPoolManager>
{

    public List<ObjectPoolItem> itemsToPool;
    public List<GameObject> pooledObjects;
    public List<GameObject> pooledAudio;
    public Transform audioParent;
    List<AudioTrackEntry> bgm;
    List<AudioTrackEntry> sfx;

    void Start()
    {
        pooledObjects = new List<GameObject>();
        foreach (ObjectPoolItem item in itemsToPool)
        {
            for (int i = 0; i < item.amountToPool; i++)
            {
                GameObject obj = (GameObject)Instantiate(item.objectToPool);
                obj.AddComponent<PooledObjectItem>();
                obj.GetComponent<PooledObjectItem>().SetID(item.id);
                obj.transform.SetParent(item.parent);
                obj.SetActive(false);
                pooledObjects.Add(obj);
            }
        }
        bgm = AudioManager.Instance.bgmTracks;
        sfx = AudioManager.Instance.sfxTracks;

        InitializeAudioList(bgm);
        InitializeAudioList(sfx);

    }

    private void InitializeAudioList(List<AudioTrackEntry> list)
    {
        foreach (AudioTrackEntry ate in list)
        {
            for (int i = 0; i < ate.amountToPool; i++)
            {
                GameObject go = Instantiate(Resources.Load("AudioTrack", typeof(GameObject))) as GameObject;
                AudioTrackEntryController atec = go.GetComponent<AudioTrackEntryController>();
                atec.InitializeAudioTrack(ate);
                go.name = ate.trackName;
                go.SetActive(false);
                if (ate.parentTransform == null)
                    go.transform.SetParent(audioParent);
                else
                    go.transform.SetParent(ate.parentTransform);
                pooledAudio.Add(go);
            }
        }
    }

    private GameObject CheckAudioListExpand(string id, List<AudioTrackEntry> list)
    {
        foreach (AudioTrackEntry item in list)
        {
            if (item.trackName == id)
            {
                if (item.shouldExpand)
                {
                    GameObject go = Instantiate(Resources.Load("AudioTrack", typeof(GameObject))) as GameObject;
                    AudioTrackEntryController atec = go.GetComponent<AudioTrackEntryController>();
                    atec.InitializeAudioTrack(item);
                    go.name = item.trackName;
                    go.SetActive(false);
                    if (item.parentTransform == null)
                        go.transform.SetParent(audioParent);
                    else
                        go.transform.SetParent(item.parentTransform);
                    pooledAudio.Add(go);
                    return go;
                }
            }
        }
        return null;
    }
    public Transform GetObjectParent(string id)
    {
        ObjectPoolItem pooledObject = itemsToPool.Find(p => p.id == id);
        return pooledObject.parent;
    }

    public ObjectPoolItem GetPooledObjectData(string id)
    {
        ObjectPoolItem pooledObject = itemsToPool.Find(p => p.id == id);
        return pooledObject;
    }

    public GameObject GetPooledObjectByTag(string tag)
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].tag == tag)
            {
                return pooledObjects[i];
            }
        }
        foreach (ObjectPoolItem item in itemsToPool)
        {
            if (item.objectToPool.tag == tag)
            {
                if (item.shouldExpand)
                {
                    GameObject obj = (GameObject)Instantiate(item.objectToPool);
                    obj.SetActive(false);
                    pooledObjects.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }

    public GameObject GetPooledObjectByID(string id)
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].GetComponent<PooledObjectItem>().GetID() == id)
            {
                return pooledObjects[i];
            }
        }

        foreach (ObjectPoolItem item in itemsToPool)
        {
            if (item.id == id)
            {
                if (item.shouldExpand)
                {
                    GameObject obj = (GameObject)Instantiate(item.objectToPool);
                    obj.AddComponent<PooledObjectItem>();
                    obj.GetComponent<PooledObjectItem>().SetID(item.id);
                    obj.transform.parent = item.parent;
                    obj.SetActive(false);
                    pooledObjects.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }

    public GameObject GetPooledAudio(string id)
    {
        for (int i = 0; i < pooledAudio.Count; i++)
        {
            if (!pooledAudio[i].activeInHierarchy && pooledAudio[i].GetComponent<AudioTrackEntryController>().trackName == id)
            {
                return pooledAudio[i];
            }
        }

        CheckAudioListExpand(id, bgm);
        CheckAudioListExpand(id, sfx);

        return null;
    }

    public Transform GetParentOfObjectID(string id)
    {
        for (int i = 0; i < itemsToPool.Count; i++)
        {
            if (itemsToPool[i].id == id)
            {
                return itemsToPool[i].parent;
            }
        }
        return null;
    }

}
