﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class LoggingManager : Singleton<LoggingManager> {

    [Tooltip("Default file directory is App's data directory")]
    [SerializeField]
    private string playerLogDirectoryPath;
    [Tooltip("Default file name is (Date Today).txt")]
    [SerializeField]
    private string fileName;

    public string PlayerLogDirectoryPath
    {
        get { return playerLogDirectoryPath; }
        set { playerLogDirectoryPath = value; }
    }
    public string FileName
    {
        get { return fileName; }
        set { fileName = value; }
    }

    private StreamWriter writer = null;

    protected override void Awake()
    {
        base.Awake();
        if (String.IsNullOrEmpty(playerLogDirectoryPath))
        {
            playerLogDirectoryPath = Application.persistentDataPath;
        }
    }

    public void Log(string message)
    {
        Log(message, "");
    }

    public void Log(string message, Exception error)
    {
        Log(message, error.Message);
    }

    public void Log(string message, string error)
    {
        createDirectoryIfNotExist(PlayerLogDirectoryPath);

        while (writer != null) { }

        write(message, error);

    }

    private void write(string message, string error)
    {
        StringBuilder sb = new StringBuilder();

        string finalFileName = PlayerLogDirectoryPath + (String.IsNullOrEmpty(fileName) ?
            DateTime.Now.ToString("MM-dd-yyyy")
            :
            fileName) +
            ".txt";

        writer = new StreamWriter(finalFileName, true);

        sb.Append(DateTime.Now.ToString("HH:mm:ss:fff") + ": " + message + Environment.NewLine);

        if (error != String.Empty)
            sb.Append("Reason: " + error + Environment.NewLine);

        writer.Write(sb.ToString());

        writer.Close();
        writer.Dispose();
        writer = null;

    }

    private void createDirectoryIfNotExist(string defaultLogPath)
    {
        if (!Directory.Exists(defaultLogPath))
            Directory.CreateDirectory(defaultLogPath);

    }

    protected override void OnApplicationQuit()
    {
        Log("Application Quit.");
    }

}