﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreText : MonoBehaviour
{
    private Text scoreText;
    public float animateDuration = 0.5f;

    [SerializeField]
    int originalScore = 0;
    [SerializeField]
    int targetScore = 0;
    private void Awake()
    {
        scoreText = GetComponent<Text>();
        ScoreManager.OnScoreUpdate += UpdateScore;
    }

    private void OnDestroy()
    {
        ScoreManager.OnScoreUpdate -= UpdateScore;
    }

    void UpdateScore(int value)
    {
        originalScore = targetScore;
        targetScore = value;
        StartCoroutine(AnimateText());
    }

    IEnumerator AnimateText()
    {
        int score = 0;
        for (float timer = 0; timer < animateDuration; timer += Time.deltaTime)
        {
            float progress = timer / animateDuration;
            score = (int)Mathf.Lerp(originalScore, targetScore, progress);
            scoreText.text = "SCORE: " + (score + 1).ToString();
            yield return null;
        }

    }
}
