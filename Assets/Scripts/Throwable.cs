﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throwable : MonoBehaviour
{

    Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
        
    private void OnEnable()
    {
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = Vector3.zero;

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<Target>() != null)
        {
            ScoreManager.Instance.AddScore(5);
        }
    }
}
