﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public float moveSpeed;
    public float minSpeed = 3.0f, maxSpeed = 5.0f;
    float moveDirection = 1.0f;


    private void Awake()
    {
        moveSpeed = minSpeed;
    }

    void ChangeSpeed()
    {
        moveSpeed = Random.Range(minSpeed, maxSpeed);
    }

    private void Update()
    {
        transform.position += Vector3.right * moveDirection * moveSpeed * Time.deltaTime;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Boundary")
        {
            moveDirection *= -1;
            float random = Random.value;
            if(random >= 0.5f)
            {
                ChangeSpeed();
            }
        }
       
    }
}
