﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleManager : Singleton<HoleManager>
{
    public List<Hole> holes;
    public List<Hole> activeHoles;
    public float minInterval = 1.0f, maxInterval = 3.0f;
    private float interval;

    float timeElapsed = 0;


    private void Start()
    {
        interval = minInterval;
    }

    private void Update()
    {
        timeElapsed += Time.deltaTime;
        if(timeElapsed > interval)
        {
            Show();
            interval = Random.Range(minInterval, maxInterval);
            timeElapsed = 0;
        }
    }

    void Show()
    {
        if (activeHoles.Count >= holes.Count)
            return;
            
            int index = Random.Range(0, holes.Count);

        while (activeHoles.Contains(holes[index]))
        {
            index = Random.Range(0, holes.Count);
        }

        holes[index].Show();
        activeHoles.Add(holes[index]);
    }

    public void RemoveActiveHole(Hole hole)
    {
        activeHoles.Remove(hole);
    }
}
